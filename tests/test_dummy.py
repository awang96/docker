import unittest
from dummy import dummy


class Test_Dummy(unittest.TestCase):

    def test_dummy(self):
        self.assertEqual(dummy(), 42)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Test_Dummy)
    unittest.TextTestRunner(verbosity=2).run(suite)