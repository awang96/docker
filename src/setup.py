from setuptools import setup, find_packages
import dummy

setup(
    name='dummy',
    version=dummy.__version__,
    packages=find_packages(),
    license="None",
    entry_points={
        'console_scripts': [
            'dummy = dummy.core:dummy'
        ]
    },
    description="Dummy package"
)
